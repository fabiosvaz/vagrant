#!/usr/bin/env bash

#===============================================================================
# Run Update and Upgrade
#===============================================================================
echo "#############Running Update and Upgrade#############"
apt-get -y update
apt-get -y upgrade

#===============================================================================
# Install unity-tweak-tool
#===============================================================================
echo "#############Install unity-tweak-tool#############"
apt-get -y install unity-tweak-tool gnome-tweak-tool

#===============================================================================
# Configure timezone
#===============================================================================
echo "#############Configure timezone#############"
timedatectl set-timezone America/New_York
timedatectl set-ntp yes

localectl set-locale LANG="en_US.UTF-8"
locale

#===============================================================================
# Install Tools
#===============================================================================
echo "#############Install Tools#############"
apt-get -y install unzip
apt-get -y install tar
apt-get -y install git
apt-get -y install screen  #Useful if you will test Yocto with a board such as Beaglebone Black
apt-get -y install nano
apt-get -y install emacs
apt-get -y install tree

#===============================================================================
# Install python libraries
#===============================================================================
echo "#############Install python libraries#############"
apt-get -y install python-pip
pip install --upgrade pip
pip install virtualenv
apt-get -y install python3-pip

#===============================================================================
# Install Java
#===============================================================================
echo "#############Installing Java#############"
apt-get -y install default-jdk

#===============================================================================
# Install Yocto Dependencies
#===============================================================================
echo "#########Packages needed to build an image on a headless system#######"
apt-get -y install gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat

echo "#########Graphical and Eclipse Plug-In Extras #######"
apt-get -y install libsdl1.2-dev xterm

#echo "#########Documentation#######"
#apt-get -y install make xsltproc docbook-utils fop dblatex xmlto

echo "#########OpenEmbedded Selef-Test (os-selftest)#######"
apt-get -y install python-git

#If your build system has the oss4-dev package installed, you might experience QEMU build failures due to the package installing 
#its own custom /usr/include/linux/soundcard.h on the Debian system. If you run into this situation, either of the following solutions exist:
echo "#########OpenEmbedded Self-Test (os-selftest)#######"
apt-get build-dep qemu
apt-get remove oss4-dev

#===============================================================================
# Clone Yocto project
#===============================================================================
echo "#########Cloning Yocto project#######"

#create folder under vagrant user
install -d -o vagrant /home/vagrant/yocto

cd /home/vagrant/yocto

su vagrant -c 'git clone http://git.yoctoproject.org/git/poky'

BASHRC=/home/vagrant/.bashrc
BBDIR=/home/vagrant/yocto/poky/bitbake

echo '' >> $BASHRC
echo '# Yocto variables"' >> $BASHRC
echo 'BBDIR=${BBDIR}' >> $BASHRC
echo 'PATH=$PATH:$BBDIR/bin' >> $BASHRC
echo 'export PATH' >> $BASHRC

#===============================================================================
# Create sstate-chache and download folders
#===============================================================================
echo "#########Create sstate-chache and download folders#######"
install -d -o vagrant /home/vagrant/yocto/sstate-cache
install -d -o vagrant /home/vagrant/yocto/downloads

#===============================================================================
# Clone additional kernels and meta-intel
#===============================================================================

cd /home/vagrant/yocto/poky

echo "#########Metadata needed only if you are modifying and building the kernel image#######"
su vagrant -c 'git clone git://git.yoctoproject.org/meta-yocto-kernel-extras meta-yocto-kernel-extras'

echo "#########Parent layer that contains many supported BSP Layers#######"
su vagrant -c 'git clone git://git.yoctoproject.org/meta-intel.git'

#===============================================================================
# Create x86 build environment and configure it
#===============================================================================

cd /home/vagrant/yocto
su vagrant -c 'source poky/oe-init-build-env x86'
BBPATH=/home/vagrant/yocto/x86

cd ${BBPATH}

su vagrant -c '/home/vagrant/yocto/poky/bitbake/bin/bitbake-layers add-layer /home/vagrant/yocto/poky/meta-intel'

echo '' >> conf/local.conf
echo 'MACHINE = "qemux86"' >> conf/local.conf
echo 'DL_DIR = "${TOPDIR}/../downloads"' >> conf/local.conf
echo 'SSTATE_DIR = "${TOPDIR}/../sstate-cache"' >> conf/local.conf

echo "#########Rebooting VM#######"
reboot