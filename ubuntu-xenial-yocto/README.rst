Ubuntu Xenial 16.04 for Yocto project
=====================================================

.. contents::
   :local:

Introduction
------------

`Yocto`_ is an open-source project that delivers a set of tools that create operating system images for embedded Linux systems.

This is a Virtualbox containing Ubuntu Xenial 16.04 and Yocto project

Software Requirements
---------------------

- `Virtualbox and VirtualBox Extension Pack <https://www.virtualbox.org/wiki/Downloads>`_
- `Vagrant <http://downloads.vagrantup.com/>`_


Getting started
---------------

Open a command line of your preference and type::

	user@host> vagrant up

It will download the boxcutter/ubuntu1604-desktop box from Atlas Hashicorp (In case you don't have) and create a new VM with the following spec::
	
	config.vm.provision :shell, path: "provision.sh"
	config.vm.boot_timeout=600

	vb.memory = "4096"
	vb.cpus = 4
	
A new VM gui will be opened, but you can also do a ssh to the new VM::

	user@host> vagrant ssh

Provisioning
---------------	

After Vagrant creates the new VM, the `<provision.sh>`_ script will be executed. You can modify it as you wish. The provisioning will be done as following::

	Run Update and Upgrade
	Install unity-tweak-tool
	Configure timezone
	Install Tools such as tar git unzip
	Install python libraries
	Install Java
	Install Yocto Dependencies
	Clone Yocto project to ~/yocto
	Create sstate-chache and download folders
	Clone additional kernels and meta-intel
	Create x86 build environment and configure it (~/yocto/x86)

Getting started with Yocto
--------------------------

Once you get your VM up, you can try to build an image and use QEMU to emulate it. Note, as you are using a VM to build an image, it might take a few hours for the first time::

	cd /home/vagrant/yocto
	source poky/oe-init-build-env x86    # The provision script already create the x86 build env. This source will init it.
	bitbake <target>  # Give a try to: core-image-sato

When you get the build done, you can emulate the image using QEMU::

	runqemu qemux86    # QEMU was already configured by the provision script to be the MACHINE option in conf/layer.conf

Available targets::

	Common targets are:
         core-image-minimal
         core-image-sato
         meta-toolchain
         meta-ide-support
	
Useful Links
-------------

- `Vagrant Docs <http://docs.vagrantup.com/v2/>`_
- `Vagrant Boxes <https://atlas.hashicorp.com/boxes/search>`_
- `Virtualbox Docs <https://www.virtualbox.org/wiki/Documentation>`_
- `Yocto Project <https://www.yoctoproject.org>`_

.. _Yocto: https://www.yoctoproject.org/