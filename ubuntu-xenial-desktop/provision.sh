#!/usr/bin/env bash

#===============================================================================
# Run Update and Upgrade
#===============================================================================
echo "#############Running Update and Upgrade#############"
apt-get -y update
apt-get -y upgrade

#===============================================================================
# Install unity-tweak-tool
#===============================================================================
echo "#############Install unity-tweak-tool#############"
apt-get -y install unity-tweak-tool gnome-tweak-tool

#===============================================================================
# Configure timezone
#===============================================================================
echo "#############Configure timezone#############"
timedatectl set-timezone America/New_York
timedatectl set-ntp yes

localectl set-locale LANG="en_US.UTF-8"
locale

#===============================================================================
# Install Tools
#===============================================================================
echo "#############Install Tools#############"
apt-get -y install unzip
apt-get -y install tar
apt-get -y install git
apt-get -y install nano
apt-get -y install emacs
apt-get -y install tree

#===============================================================================
# Install python libraries
#===============================================================================
echo "#############Install python libraries#############"
apt-get -y install python-pip
pip install --upgrade pip
pip install virtualenv
apt-get -y install python3-pip

#===============================================================================
# Install Java
#===============================================================================
echo "#############Installing Java#############"
apt-get -y install default-jdk


echo "#########Rebooting VM#######"
reboot