#!/usr/bin/env bash

#===============================================================================
# Run Update and Upgrade
#===============================================================================
echo "#############Running Update and Upgrade#############"
apt-get -y update
apt-get -y upgrade

#===============================================================================
# Install unity-tweak-tool
#===============================================================================
echo "#############Install unity-tweak-tool#############"
apt-get -y install unity-tweak-tool gnome-tweak-tool

#===============================================================================
# Configure timezone
#===============================================================================
echo "#############Configure timezone#############"
timedatectl set-timezone America/New_York
timedatectl set-ntp yes

localectl set-locale LANG="en_US.UTF-8"
locale

#===============================================================================
# Install Tools
#===============================================================================
echo "#############Install General Tools#############"
apt-get -y install unzip
apt-get -y install tar
apt-get -y install git
apt-get -y install screen  #Useful if you will test Yocto with a board such as Beaglebone Black
apt-get -y install nano
apt-get -y install emacs
apt-get -y install tree

echo "#############Installing Wireshark#############"
add-apt-repository ppa:wireshark-dev/stable
apt-get -y update
apt-get -y install tshark
apt-get -y install wireshark
apt-get -y install wireshark-doc

echo "#############Installing Mininet#############"
apt-get -y install mininet

#===============================================================================
# Install python libraries
#===============================================================================
echo "#############Install python libraries#############"
apt-get -y install python-pip
pip install --upgrade pip
pip install virtualenv
apt-get -y install python3-pip

#===============================================================================
# Install Open vSwitch
#===============================================================================
echo "#########Installing Open vSwitch#######"
apt-get -y install openvswitch-common
apt-get -y install openvswitch-switch
apt-get -y install openvswitch-test
apt-get -y install openvswitch-testcontroller
apt-get -y install python-openvswitch


#===============================================================================
# Install OpendayLight Project
#===============================================================================
echo "#########Installing OpendayLight#######"

#create folder under vagrant user
install -d -o vagrant /home/vagrant/opendaylight

cd /home/vagrant/opendaylight

wget -nd -c "https://nexus.opendaylight.org/content/groups/public/org/opendaylight/integration/distribution-karaf/0.4.1-Beryllium-SR1/distribution-karaf-0.4.1-Beryllium-SR1.tar.gz"
su vagrant -c 'tar -xf distribution-karaf-0.4.1-Beryllium-SR1.tar.gz'

echo "#########Rebooting VM#######"
reboot
