Ubuntu Xenial 16.04 for OpendayLight Plataform
=====================================================

.. contents::
   :local:

Introduction
------------

`OpendayLight`_ is a collaborative open source project hosted by The Linux Foundation. The goal of the project is to accelerate the adoption of software-defined networking (SDN) 
and create a solid foundation for Network Functions Virtualization (NFV). The software is written in Java.

This is a Virtualbox containing Ubuntu Xenial 16.04 and OpendayLight project

Software Requirements
---------------------

- `Virtualbox and VirtualBox Extension Pack <https://www.virtualbox.org/wiki/Downloads>`_
- `Vagrant <http://downloads.vagrantup.com/>`_


Getting started
---------------

Open a command line of your preference and type::

	user@host> vagrant up

It will download the boxcutter/ubuntu1604-desktop box from Atlas Hashicorp (In case you don't have) and create a new VM with the following spec::
	
	config.vm.provision :shell, path: "provision.sh"
	config.vm.boot_timeout=600

	vb.memory = "4096"
	vb.cpus = 4
	
A new VM gui will be opened, but you can also do a ssh to the new VM::

	user@host> vagrant ssh

Provisioning
---------------	

After Vagrant creates the new VM, the `<provision.sh>`_ script will be executed. You can modify it as you wish. The provisioning will be done as following::

	Run Update and Upgrade
	Install unity-tweak-tool
	Configure timezone
	Install Tools such as tar git unzip
	Intall Wireshark and Mininet
	Install python libraries
	Install Java
	Install Open vSwitch
	Install OpendayLight Project

Getting started with OpendayLight
--------------------------

Once you get your VM up, you can start OpendayLight::

	cd /home/vagrant/opendaylight/distribution-karaf-0.4.1-Beryllium-SR1
	./bin.karaf

Install necessary features::

	feature:install -v odl-restconf odl-l2switch odl-mdsal-apidocs odl-dlux-core odl-dlux-all odl-dlux-yangui

Access OpendayLight::

	http://localhost:8181/index.html

Useful Links
-------------

- `Vagrant Docs <http://docs.vagrantup.com/v2/>`_
- `Vagrant Boxes <https://atlas.hashicorp.com/boxes/search>`_
- `Virtualbox Docs <https://www.virtualbox.org/wiki/Documentation>`_
- `OpendayLight Project <https://www.opendaylight.org>`_

.. _OpendayLight: https://www.opendaylight.org/
